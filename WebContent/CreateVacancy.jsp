


<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Vacancies</title>
<style>
.jumbo {
    position: relative;
    background-size: cover;
    height : 80px;
    width : 1140px;
    overflow: hidden;
    background-color:#126180;
}
.con{
position : absolute;
top : 900px;
width :1287px;
align : center;
}

</style>
</head>
<body class = "body">
<div class="container">
<div class="col-sm-4"></div>
<div class="jumbo">
    <h2 align = center style = "color : white"><b><i>Online Recruitment System</i></b></h2>            
</div>
<div class = 'well-lg'>

<div class="container" style="margin: -100px 0px 70px 0px">
<div class="col-sm-4"></div>


<div class="col-sm-4" style="margin-top: 100px">

<form action="VacancyController" >
<table align="center" class="table table-striped">
<thead>
<tr><th colspan="3" align="center"><h2>Create new vacancy</h2></th></tr></thead>
<tr><td>Job title:</td><td><input type = text name = "jobtitle" required></td></tr>
<!-- <input type="text" list="countries" /> -->
<!-- <datalist id="countries"> -->
</td></tr>
<tr><td>Number Of Vacancies:</td><td><input type = text name = vacancies required></td></tr>
<tr><td>Job Description:</td><td><input type = text name = jobdescription required></td></tr>
<tr><td>Work Experience:</td><td><input type = text name = workexperience required></td></tr>
<tr><td>Location of Jobs:</td><td><select name = location required> 
	<option value="Hyderabad">Hyderabad</option>
    <option value="Chennai">Chennai</option>
    <option value="Bangalore">Bangalore</option>
    <option value="Coimbatore">Coimbatore</option></td></tr>

<tr><td>Qualification</td><td><select name = qualification required> 
	<option value="M.C.A(Computers)">M.C.A(Computers)</option>
	<option value="M.Tech(Computers)">M.Tech(Computers)</option>
	<option value="Any Graduate(MBA Preferred)">Any Graduate(MBA Preferred)</option>
<tr><td>Status</td> <td><select name = status required> 
	<option value="Vacency open">Vacancy open</option>
	<option value="Vacency closed">Vacancy closed</option>
	</select></td></tr>
	

<tr><td colspan="6" align="center"><a href = "Viewvacancies.jsp"><input type = submit value = Create></a></td></tr>
<!-- td colspan="5" align="center"><a href="WelcomePage.view"><input type = button value = back></a></td> -->
</table>
</form>
<%
String msg = request.getParameter("msg");
if(msg != null){
	out.print("<table align=center><tr><td><font color=red>*" +  msg + "</font color></td></tr></table>");
}
%>
</div>
</div>
</div>

</body>
</html>

