<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Vacancies</title>
<style>
.jumbo {
    position: relative;
    background-size: cover;
    height : 80px;
    width : 1140px;
    overflow: hidden;
    background-color:#126180;
}
.con{
position : absolute;
top : 900px;
width :1287px;
align : center;
}

</style>
</head>
<body class = "body">
<div class="container">
<div class="col-sm-4"></div>
<div class="jumbo">
    <h2 align = center style = "color : white"><b><i>Online Recruitment System</i></b></h2>            
</div>
<a href = "userprofile.jsp">Profile</a><a href = "#">>></a><a href = "profilevacancies.jsp">Vacancies</a><a href = "#">>></a>
<a href = "profilevacancy3.jsp">Business Development Manager </a>
<div class = "case1">
<h3><font color = "olive"><i><b>Business Development Manager </b></i></font></h3>
<div>Number of People Required : 5
</div><br>

<div>Qualification :  Any Graduate (MBA Preferred)
</div><br>

<div>Responsibilities/Job Description:
</div><br>
<div> 
<ul>
    <li>Responsible for alliances with colleges for Super Campus business programmes </li>
    <li> Building a strong relationship with college leadership and students</li>
    <li>Solid experience in feet-on-street-below the line (BTL) marketing activities ,Must be able to travel to colleges </li>
</ul>
</div> <br>
<div>Years of experience required : 3+ years of experience in teaching or similar role</div><br>

<div>Location : Chennai/West Bengal/Bhopal/Kerala/Warangal</div><br>

<div>Skill sets required:
<ul>
<li>Good communication skills</li>
<li>Should be go- getter and proactive approach</li>
</ul>
</div>
<div>
<a href = "resume.jsp"><h4>Apply for the job</h4></a>
</div>

</div>
</div>
<div class="con">
    <div class="panel panel-default">
    <div class="panel-body">Copyright © TalentSprint, 2015. All Rights Reserved.</div>
  </div>
</div>

</body>
</html>