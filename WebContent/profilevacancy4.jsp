<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Vacancies</title>
<style>
.jumbo {
    position: relative;
    background-size: cover;
    height : 80px;
    width : 1140px;
    overflow: hidden;
    background-color:#126180;
}
.con{
position : absolute;
top : 900px;
width :1287px;
align : center;
}

</style>
</head>
<body class = "body">
<div class="container">
<div class="col-sm-4"></div>
<div class="jumbo">
    <h2 align = center style = "color : white"><b><i>Online Recruitment System</i></b></h2>            
</div>
<a href = "userprofile.jsp">Profile</a><a href = "#">>></a><a href = "profilevacancies.jsp">Vacancies</a><a href = "#">>></a>
<a href = "profilevacancy4.jsp"> Quantitative Aptitude Faculty </a>
<div class = "case1">
<h3><font color = "olive"><i><b> Quantitative Aptitude Faculty </b></i></font></h3>
<div>Number of People Required : 5
</div><br>

<div>Responsibilities/Job Description:
</div><br>
<div> 
<ul>
    <li>Responsible for Training the Students on Quantitative Aptitude in preparation for the Common written Examination</li>
    <li>Creation of necessary content for training</li>
    <li>Delivery of Training</li>
    <li>Putting in place measurement metrics for measuring the performance of the trainees</li>
    <li>Keeping abreast of various developments in the Banking entrance examination and training students for the same </li>
    <li>Conduct periodic Tests and review / analysis of performance by Students</li>
    <li>Setting Question papers for Mock Tests and Full Tests – across varying levels of difficulties</li>
</ul>
</div> <br>
<div>Years of experience required :  Minimum 3 - 8 years of experience with atleast 2 years as Quantitative Aptitude Trainer. Experience in Training for Bank PO a plus</div><br>

<div>Location : Hyderabad/Chennai/Bangalore/Coimbatore. But must be flexible to travel for training to other locations</div><br>

<div>Skill sets required:
<ul>
<li>Training experience on Aptitude and Quantitative Skills</li>
<li>Ability to set question papers on varying difficulties in order to get the student prepared for the CWE</li>
<li> Managing other members in the Quant and Aptitude Training for effective implementation of the program</li>
</ul>
</div>
<div>
<a href = "resume.jsp"><h3>Apply for the job</h3></a>
</div>

</div>
</div>
<div class="con">
    <div class="panel panel-default">
    <div class="panel-body">Copyright © TalentSprint, 2015. All Rights Reserved.</div>
  </div>
</div>

</body>
</html>