<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
    <%
    String number = request.getParameter("number");
    String name = request.getParameter("question");
    String QA = request.getParameter("option1");
    String QB = request.getParameter("option2");
    String QC = request.getParameter("option3");
    String QD = request.getParameter("option4");
    String correctAns = request.getParameter("answer");
    %>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>


<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Update Question</title>


<style>

.jumbo {
position: relative;
background-size: cover;
height : 80px;
width : 1140px;
overflow: hidden;
background-color:#126180;
}

.jumbotron{
position : absolute;
top : 130px;
left-margin : 370px;
}

</style>


</head>
<body>

<div class="container">
<div class="col-sm-4"></div>
<div class="jumbo">
<h2 align = center style = "color : white"><b><i>Online Recruitment System</i></b></h2>
</div>
</div>

<div class="container">
<div class="jumbotron" style="margin-left: 180px">

<center>
<form action = "UpdateQuestionController">
<table class = "table" border="0">
<thead>
<tr>
<th colspan="2"><h3 align = "center">Update Question</h3>
</th>
</tr>
</thead>
<tr><td>Question no.&nbsp;&nbsp;<input type = text name = number value="<%=number%>" readonly = "readonly"></td></tr>
<tr><td>Question&nbsp;&nbsp;<textarea rows="5" cols="100" name=question value="<%=name%>"></textarea></td></tr>
<tr><td>A&nbsp;&nbsp;<input type  = text  name = option1 value="<%=QA%>"></td></tr>
<tr><td>B&nbsp;&nbsp;<input type  = text  name = option2 value="<%=QB%>"></td></tr>
<tr><td>C&nbsp;&nbsp;<input type  = text  name = option3 value="<%=QC%>"></td></tr>
<tr><td>D&nbsp;&nbsp;<input type  = text  name = option4 value="<%=QD%>"></td></tr>
<tr><td>correct&nbsp;&nbsp;<select name= "answer" value="<%=correctAns %>">
  <option value="QA">A</option>
  <option value="QB">B</option>
  <option value="QC">C</option>
  <option value="QD">D</option>
</select></td></tr>
<tr><td>&nbsp;&nbsp;&nbsp;<input type = submit value = Save></td></tr>
</table>
</form>
</center>
</div>
</div>
</body>
</html>