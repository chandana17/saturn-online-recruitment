<%@page import="java.io.PrintWriter"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%> 
<%@page import = "com.bvrit.ors.beans.*" %>
<%@ page import = "java.util.*"%>
<%@page import = "com.bvrit.ors.dao.*" %>



  
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
  <%
  String email = (String)session.getAttribute("email");
  out.print("heylo" + email);

  VacancyDAO pdao = new VacancyDAO();
  //UserDAO udao = new UserDAO();
  List<CreateVacancyBean> lst = pdao.listvacancies();
  ListIterator<CreateVacancyBean> lt = lst.listIterator();
  response.setContentType("text/html");
%> 
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<title>Vacancies</title>
</head>
<body>

<div class = 'well-lg'>
<div class="container">
<div>
<form>
<tr><th align="center"><h2>Vacancies</h2></th></tr></thead>
<table class="table table-bordered">
    <thead>
      <tr>
        <th>Job Title</th>
        <th>Num of vacancies</th>
        <th>Job Description</th>
        <th>Work experience</th>
        <th>Location</th>
        <th>Qualification</th>
        <th>Status</th>
        <th></th>
      </tr>
    </thead>


<% while(lt.hasNext()){
     CreateVacancyBean pb = lt.next();
     //UserBean ub = new UserBean();
     out.print("<tbody>");
     out.print("<tr>");
     out.print("<td>"+pb.getJobtitle()+"</td>");
     out.print("<td>"+pb.getVacancies()+"</td>");
     out.print("<td>"+pb.getJobdescription()+"</td>");
     out.print("<td>"+pb.getWorkexperience()+"</td>");
     out.print("<td>"+pb.getLocation()+"</td>");
     out.print("<td>"+pb.getQualification()+"</td>");
     out.print("<td>"+pb.getStatus()+"</td>");
     out.print("<td>"+"<a href = application.jsp?email="+email+">Apply for Job</a>"+"</td>");
     out.print("</tr>");
     out.print("</tbody>");

}
%>
</table>
</form>
</div>
</div>
</div>
 
</body>
</html>
