
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
 
 <%

String message = request.getParameter("message");
if(message != null)
	out.print(message);
%>
 

<html>
<head>


<script type="text/javascript"> 
function checkForm(form) {

if(form.password.value != "" && form.npassword.value == form.rpassword.value)
{ 
 if(form.npassword.value.length < 6)
 { 
 alert("Error: Password must contain at least six  to ten characters!");
 form.npassword.focus(); 
 return false;
 } 
 
  
 re = /[0-9]/; if(!re.test(form.npassword.value)) 
 { 
 alert("Error: password must contain at least one number (0-9)!"); 
 form.npassword.focus(); 
 return false;
 }
 
 re = /[a-z]/;
 if(!re.test(form.npassword.value)) 
 { 
 alert("Error: password must contain at least one lowercase letter (a-z)!"); 
 form.npassword.focus();
 return false; 
 }
 re = /[A-Z]/;
 if(!re.test(form.npassword.value)) 
 { 
 alert("Error: password must contain at least one uppercase letter (A-Z)!"); 
 form.npassword.focus(); 
 return false;
 } 
 re = /[!@#$%^&*()]/;
 if(!re.test(form.npassword.value)) 
 { 
 alert("Error: password must contain at least one Special Character"); 
 form.npassword.focus(); 
 return false;
 } 
 } 
 else 
 { 
 alert("Error: Please check that you've entered and confirmed your password!"); 
 form.npassword.focus(); 
 return false; 
 } 
 alert("You entered a valid password");
 return true;
 } 
     
 </script>

 

<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Online Recruitment System</title>
<style>
.jumbo {
    position: relative;
    background-size: cover;
    height : 80px;
    width : 1140px;
    overflow: hidden;
    background-color:#126180;
}
.jumbotron{
   position : absolute;
   top : 150px;
   left-margin : 470px;
   width : 600px;
   height :400px;
}


.con{
position : absolute;
top : 900px;
width :1287px;
align : center;
}


</style>
</head>

<body class = "body">
<div class="container">
<div class="col-sm-4"></div>
<div class="jumbo">
    <h2 align = center style = "color : white"><b><i>Online Recruitment System</i></b></h2>            
</div>

<div class="container">
<div class="jumbotron" style="margin-left: 270px">
 
<form action = "UploadServlet" onsubmit="return checkForm(this);" >
<table class="table" >
<thead>
<tr><td colspan="2" align="center"><h2>Edit Password</h2></td></tr></thead>
<tr><td><b>Old Password:</b></td><td><input type = password name = password ></td></tr>
<tr><td><b>New Password:</b></td><td><input type = password name = npassword></td></tr>
<tr><td><b>Re-Type Password:</b></td><td><input type = password name = rpassword></td></tr>
<tr><td align="center"><input type = submit value = Submit></td></tr>
</table>
</form>
</div>
</div>
</div>
<div class="con">
    <div class="panel panel-default">
    <div class="panel-body">Copyright � TalentSprint, 2015. All Rights Reserved.</div>
  </div>
</div>
</body>
</html>

