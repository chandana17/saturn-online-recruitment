<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Profile</title>
<style>
.jumbotron {
    position: relative;
    background-size: cover;
    height : 150px;
    width : 1200px;
    overflow: hidden;
    background-color:#126180; 
}
.jumbotronwidth{
top :0px;
max-width : 30%;
}
.navbar{
top : 35px;
background-color: #126180;
}

</style>
</head>

<body class = "body">
<div class="container">
<div class="col-sm-4"></div>
<div class="jumbotron">
    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 pull-right "> <img vspace="20" class="jumbotronwidth"  src="./images/talentsprintimg.jpg"> </div>
    <h2 align = center style = "color : white"><b><i>Online Recruitment System</i></b></h2>                        
  </div>            
</div>

 <nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="empprofile.jsp"><font color = "white"><b>Profile</b></a>
    </div>
    <div>
      <ul class="nav navbar-nav">

        <li class="dropdown"><a href="ListQuestion.jsp"><font color = white><b>List of Questions</b></font></a>
        </li>  
          <li class="dropdown"><a href="questions.jsp"><font color = white><b>Online test Papers</b></font></a>
        </li>
        <li class="dropdown"><a href="CreateVacancy.jsp"><font color = white><b>Create Vacancy</b></font></a>
        </li>
        <li class="dropdown"><a href="Viewvacancies.jsp"><font color = white><b>View Vacancies</b></font></a>
        </li>
      </ul>
      
      <ul class="nav navbar-nav navbar-right">
        <li class="dropdown"><a href="Mainpage.jsp"><span class="glyphicon glyphicon-log-out"></span> <font color = white><b>Logout</b></font></a>
        </li>
      </ul>
      
    </div>
  </div>
</nav>

</body>
</html>