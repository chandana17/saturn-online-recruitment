<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Adding Questions</title>

<style>

.jumbo {
position: relative;
background-size: cover;
height : 80px;
width : 1140px;
overflow: hidden;
background-color:#126180;
}

.jumbotron{
position : absolute;
top : 130px;
left-margin : 370px;
}

</style>
</head>
<body>

<div class="container">
<div class="col-sm-4"></div>
<div class="jumbo">
<h2 align = center style = "color : white"><b><i>Online Recruitment System</i></b></h2>
</div>
</div>

<div class="container">
<div class="jumbotron" style="margin-left: 180px">

<center>
<form action="AddQuestionController">
<table class = "table" border="0">
<thead>
<tr>
<th colspan="2"><h3 align = "center">Add Question</h3></th>
</tr>
</thead>
<tr><td>Question no.&nbsp;&nbsp;<input type = text name = number></td></tr>
<tr><td>Question&nbsp;&nbsp;<textarea rows="5" cols="100" name=question></textarea></td></tr>
<tr><td>A&nbsp;&nbsp;<input type  = text  name = option1></td></tr>
<tr><td>B&nbsp;&nbsp;<input type  = text  name = option2></td></tr>
<tr><td>C&nbsp;&nbsp;<input type  = text  name = option3></td></tr>
<tr><td>D&nbsp;&nbsp;<input type  = text  name = option4></td></tr>
<tr><td>correct&nbsp;&nbsp;<select name= "answer">
  <option value="QA">A</option>
  <option value="QB">B</option>
  <option value="QC">C</option>
  <option value="QD">D</option>
</select> </td></tr>
<tr><td><input type = submit value = AddQuestion> &nbsp;&nbsp;&nbsp;</td></tr>
</table>
</form>
</center>
</div>
</div>
</body>
</html>