<!DOCTYPE html>
<html>
<head>
 <title>Blog</title>
	    <link href="css/bootstrap.css" rel='stylesheet' type='text/css'/>
	    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
         <script src="js/jquery.min.js"></script>
	    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="keywords" content="Payroll Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template,
		Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
		<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
		<link rel="stylesheet" href="css/flexslider.css" type="text/css" media="screen" />
		<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
		<link href='http://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700,300italic,400italic,500italic,700italic' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Bree+Serif' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Anton' rel='stylesheet' type='text/css'>
        <!---- start-smoth-scrolling---->
		<script type="text/javascript" src="js/move-top.js"></script>
		<script type="text/javascript" src="js/easing.js"></script>
		<script type="text/javascript">
			jQuery(document).ready(function($) {
				$(".scroll").click(function(event){		
					event.preventDefault();
					$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
				});
			});
		</script>
		<script src="js/jquery.easydropdown.js"></script>
			<script src="js/cbpViewModeSwitch.js" type="text/javascript"></script>
</head>
<body>
 
				<div class="header">
				 <div class="container">	
				   <div class="logo">
					<a href="index.html"><img src="images/talentsprintimg.jpg" width = 150 height = 50 alt=""/></a>
				</div>
					<div class="top-menu">
						<span class="menu"> </span>
						<ul>
							<li><a  href="Mainpage.jsp">home</a></li>
							<li><a href="Vacancies.jsp">Vacancies</a></li>
							<li><a class="active" href="aboutus.jsp">About Us<lable><img src="images/line.png"></lable></a></li>
							<li><a href="signup.jsp">SignUp</a></li>
							
							<li><a class="last" href="Blog.jsp">blog</a></li>
							</ul>
							</div>
							<script>
			$("span.menu").click(function(){
				$(".top-menu ul").slideToggle("slow" , function(){
				});
			});
		</script>
		<!-- script-for-menu -->
							
					<!-- search-scripts -->
					<script src="js/classie.js"></script>
					<script src="js/uisearch.js"></script>
						<script>
							new UISearch( document.getElementById( 'sb-search' ) );
						</script>
					<!-- //search-scripts -->
				<div class="clearfix"></div>
			</div>

		<!-- script-for-nav -->
				<script>
					$(document).ready(function(){
						$("span.menu").click(function(){
							$(".top-nav ul").slideToggle(1000);
						});
					});
				</script>
		<!-- script-for-nav -->
		<!-- /top-nav -->
                   <div class="clearfix"></div>
				</div>
		 </div>
		  <!-- header-section-ends -->
		  <div class="banner">
		  	<div class="container">
		  	<h3>Blog</h3>
		  	</div>
		  	</div>
		  	<div class="content">
		  	<div class="about-text">
		  	
		  		<p><h3><b>Job Search Advice and Recruiting Tips</b></h3></p>
<div class="container">
  <div class="btn-group">
    
		  		<br>
		  		<a href = "Blog.jsp"><button type="button" class="btn btn-primary">Job Seekers</button></a>
               <a href = "rblog.jsp"><button type="button" class="btn btn-primary">Recruiters</button></a>
		  		</div>
		  		
		  	</div>
		  	
  </div>
</div>
<div class = "case1" align = "center">

<h4><i><b><a href = "continue.jsp">The Single Biggest Lie That HR Tells Candidates!</a></b></i></h4>
<p>
No one ever wants to admit this but it can be really intimidating working with someone 
who is way smarter and more talented than you.
This is the basis for the biggest lie HR tells candidates.
"You are overqualified!"...
</p>
</div>
		  		</div>
		  		</div>
	
	<!-- <div class="good-design">
		
		<h3>we   belive   in   good   design</h3>
		<div class="gooddesign-grids">
			<div class="container">
		<div class="col-md-3 gooddesign-grid">
			<img src="images/web.png">
			<h4>Web Design</h3>
			<p>Lorem ipsum dolor sit ametconsec tetur adipiscing elit. Aliquam nulla nibh, fermentum nec interdum in, ultrices eget sapien. Ut sit amet vehicula risus. Nulla sed erat eu nisl porta bibendum. Sed tellus felis. </p>
			</div>
			<div class="col-md-3 gooddesign-grid">
			<img src="images/samrt.png">
			<h4>Phone App</h3>
			<p>Lorem ipsum dolor sit ametconsec tetur adipiscing elit. Aliquam nulla nibh, fermentum nec interdum in, ultrices eget sapien. Ut sit amet vehicula risus. Nulla sed erat eu nisl porta bibendum. Sed tellus felis. </p>
			</div>
			<div class="col-md-3 gooddesign-grid">
			<img src="images/comm.png">
			<h4>Commercial</h3>
			<p>Lorem ipsum dolor sit ametconsec tetur adipiscing elit. Aliquam nulla nibh, fermentum nec interdum in, ultrices eget sapien. Ut sit amet vehicula risus. Nulla sed erat eu nisl porta bibendum. Sed tellus felis. </p>
			</div>
			<div class="col-md-3 gooddesign-grid">
			<img src="images/med.png">
			<h4>Media Planing</h3>
			<p>Lorem ipsum dolor sit ametconsec tetur adipiscing elit. Aliquam nulla nibh, fermentum nec interdum in, ultrices eget sapien. Ut sit amet vehicula risus. Nulla sed erat eu nisl porta bibendum. Sed tellus felis. </p>
			</div>
			<div class="clearfix"></div>
		</div>
		</div>
		</div> -->
		<!-- <div class="our-team">
			<div class="container">
			<h3>our team</h3>
			<div class="ourteam-grids">
				<div class="col-md-6">
					<div class="ourteam-grid span1">
					<div class="ourteam-grid1">
						<div class="ourteam-img">
							<img src="images/t1.jpg">
							</div>
							<div class="ourteam-text">
								<h5>G.Chandana</h4>
								<h4>IT Department</h3>
								<a href="#">chandana@gmail.com</a>
								
								</div>
								<div class="clearfix"></div>
					</div>
					<p>Lorem ipsum dolor sit amet, consectetur adip iscing elitont. Ut ultricies sagittis magna acommo doinger. Ut eget erosoni mauris utcurami susurnand. Vestibulum ante ipsu  primi sini  desiturkarti edolar sitamet deakorna seni cool derrsim de peace biraz doldinNulla facilisi. Aliquam elit nisienatis achen oldu bonibkno tranto.</p>
				
				</div>
				</div>
			
				<div class="col-md-6">
					<div class="ourteam-grid span2">
					<div class="ourteam-grid1">
						<div class="ourteam-img">
							<img src="images/t3.jpg">
							</div>
							<div class="ourteam-text">
								<h5>K.Divya</h4>
								<h4>CSE Department</h3>
								<a href="#">divyareddy.kyathams@gmail.com</a>
								
								</div>
								<div class="clearfix"></div>
					</div>
					<p>Lorem ipsum dolor sit amet, consectetur adip iscing elitont. Ut ultricies sagittis magna acommo doinger. Ut eget erosoni mauris utcurami susurnand. Vestibulum ante ipsu  primi sini  desiturkarti edolar sitamet deakorna seni cool derrsim de peace biraz doldinNulla facilisi. Aliquam elit nisienatis achen oldu bonibkno tranto.</p>
				
				</div>
				</div>
				
				<div class="col-md-6">
					<div class="ourteam-grid span1">
					<div class="ourteam-grid1">
						<div class="ourteam-img">
							<img src="images/t2.jpg">
							</div>
							<div class="ourteam-text">
								<h5>M.Usha Rani</h4>
								<h4>IT Department</h3>
								<a href="#">musirikeusha@gmail.com</a>
								
								</div>
								<div class="clearfix"></div>
					</div>
					<p>Lorem ipsum dolor sit amet, consectetur adip iscing elitont. Ut ultricies sagittis magna acommo doinger. Ut eget erosoni mauris utcurami susurnand. Vestibulum ante ipsu  primi sini  desiturkarti edolar sitamet deakorna seni cool derrsim de peace biraz doldinNulla facilisi. Aliquam elit nisienatis achen oldu bonibkno tranto.</p>
				
				</div>
				</div>
				<div class="col-md-6">
					<div class="ourteam-grid span2">
					<div class="ourteam-grid1">
						<div class="ourteam-img">
							<img src="images/t4.jpg">
							</div>
							<div class="ourteam-text">
								<h5>P.Sri Ramya</h4>
								<h4>CSE Department</h3>
								<a href="#">sriramyapenmatsa@gmail.com</a>
								
								</div>
								<div class="clearfix"></div>
					</div>
					<p>Lorem ipsum dolor sit amet, consectetur adip iscing elitont. Ut ultricies sagittis magna acommo doinger. Ut eget erosoni mauris utcurami susurnand. Vestibulum ante ipsu  primi sini  desiturkarti edolar sitamet deakorna seni cool derrsim de peace biraz doldinNulla facilisi. Aliquam elit nisienatis achen oldu bonibkno tranto.</p>
				</div>
				</div>
				      <div class="clearfix"></div>
				</div>
			
		</div>
		</div> -->
				            <!-- <div class="footer">
								<div class="container">
									<div class="col-md-3 twitter-feeds">
										<h3>twitter feeds</h3>
										<div class="twitter-feed">
										<p>Check out this great theme item</p>
                                         <a href="#">http://tadjalskfj.com</a>
                                          <p>2 weeks ago</p>
                                          </div>
                                          <div class="twitter-feed">
                                          <p>Check out this great theme item</p>
                                         <a href="#">http://tadjalskfj.com</a>
                                          <p>2 weeks ago</p>
                                          </div>
                                          <div class="twitter-feed">
                                          <p>Check out this great theme item</p>
                                         <a href="#">http://tadjalskfj.com</a>
                                          <p>2 weeks ago</p>
									</div>
									</div>
									<div class="col-md-3 new-letter">
										<h3>newsletter</h3>
										<div class="new-letters">
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut ultricies sagittis magna a comm. Ut eget eros mauris, urna.</p>
										</div>
										<h4>Your Email Address</h4>
										<form>
										<input type="text">
										<input type="button" value="sign up">
										</form>
										</div>
										<div class="col-md-3 recent-projects">
											<h3>recent projects</h3>
											
												<div class="recent-project">
											<div class="recent-img">
												<img src="images/im1.jpg">
												</div>
												<div class="recent-text">
													<p>Sketchy Business Card</p>
                                                       <a href="#">http://tadjalskfj.com</a>
                                                        <p>2 weeks ago</p>
													</div>
													<div class="clearfix"></div>
													</div>
													<div class="recent-project">
													<div class="recent-img">
												<img src="images/im2.jpg">
												</div>
												<div class="recent-text">
													<p>Message Boards in Nature</p>
                                                     <a href="#">http://tadjalskfj.com</a>
                                                      <p>2 weeks ago</p>
													</div>
													<div class="clearfix"></div>
													</div>
													<div class="recent-project">
													<div class="recent-img">
												<img src="images/im3.jpg">
												</div>
												<div class="recent-text">
													<p>Website Design Kit</p>
													<a href="#">http://tadjalskfj.com</a>
													<p>2 weeks ago</p>
													</div>
													<div class="clearfix"></div>
													</div>
												
											</div>
											<div class="col-md-3 contact">
												<h3>contact</h3>
												<ul>
													<li><i class="phone"></i></li>
														<li><p>1-000-000-0000</p>
															<p>1-000-000-0000</p></li>
												</ul>
												<ul>
													<li><i class="smartphone"></i></li>
														<li><p>1-000-000-0000</p>
															<p>1-000-000-0000</p></li>
												</ul>
												<ul>
													<li><i class="message"></i></li>
														<li><a href="#">bcdefg@hijs.dfh</a>
                                                            <a href="#">fjashfaf@jkfs.ckd</a></li>
												</ul>
												</div>
									<div class="clearfix"></div>
									<div class="footer-bottom"> -->
									
									
									<br>
									<br>
				 		<p> Copyright � TalentSprint, 2015. All Rights Reserved.</p>
						
                 </div>
                  <script type="text/javascript">
						$(document).ready(function() {
							/*
							var defaults = {
					  			containerID: 'toTop', // fading element id
								containerHoverID: 'toTopHover', // fading element hover id
								scrollSpeed: 1200,
								easingType: 'linear' 
					 		};
							*/
							
							$().UItoTop({ easingType: 'easeOutQuart' });
							
						});
					</script>
				<a href="#" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
                 </div>
			</div>
</body>
</html>
