
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>Upload</title>
<style>

.jumbo {
position: relative;
background-size: cover;
height : 80px;
width : 1140px;
overflow: hidden;
background-color:#126180;
}

.jumbotron{
position : absolute;
top : 130px;
left-margin : 370px;
}

</style>

</head>
<body class = "body">
<div class="container">
<div class="col-sm-4"></div>
<div class="jumbo">
<h2 align = center style = "color : white"><b><i>Online Recruitment System</i></b></h2>
</div>

<div class="container">
<div class="jumbotron" style="margin-left: 200px">

<center>
        
        <form method="post" action="UploadServlet" enctype="multipart/form-data">
            <table class = "table" border="0">
                <thead>
                <tr>
                    <th colspan="2" ><h2 align="center"><b><i>Application Form</i></b></h2></th>
                </tr>
                </thead>
                
                <tr>
                    <td>First Name : </td>
                    <td><input type="text" name="firstName" size="50"/></td>
                </tr>
                <tr>
                    <td>Last Name : </td>
                    <td><input type="text" name="lastname" size="50"/></td>
                </tr>
                <tr>
                    <td>Email : </td>
                    <td><input type="text" name="email" size="50"/></td>
                </tr>
                <tr>
                    <td>University : </td>
                    <td><input type="text" name="university" size="50"/></td>
                </tr>
                <tr>
                    <td>Percentage : </td>
                    <td><input type="text" name="percentage" size="50"/></td>
                </tr>
                <tr>
                    <td>Experience : </td>
                    <td><input type="text" name="experience" size="50"/></td>
                </tr>
                <tr>
                    <td>Apply for job : </td>
                    <td><input type="text" name="applyforjob" size="50"/></td>
                </tr>
                <tr>
                    <td>Resume : </td>
                    <td><input type="file" name="photo" size="50"/></td>
                    
                </tr>
                <tr>
                    <td colspan="2">
                        <input type="submit" value="Save">
                    </td>
                </tr>
            </table>
        </form>
    </center>
    </div>
    </div>
</body>
</html>