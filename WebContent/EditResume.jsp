
<%@page import="java.util.ListIterator"%>

<%@page import="java.util.*"%>
<%@page import = "com.bvrit.ors.dao.*" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%     
        String name = request.getParameter("name");
        String email =  request.getParameter("email");
        String careerobjective = request.getParameter("careerobjective");
        String profilesummary = request.getParameter("profilesummary");
        String personaltraits = request.getParameter("personaltraits");
        String academia = request.getParameter("academia");
        String technicalqualification = request.getParameter("technicalqualification");
        String project = request.getParameter("project");
        String achievements = request.getParameter("achievements");
        String personaldetails = request.getParameter("personaldetails"); 

    %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
 <title> Edit Fields</title>
  <meta name="author" content="Jake Rocheleau">
  <link rel="stylesheet" type="text/css" href="style.css">
  <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
  <script type="text/javascript" src="profile.js"></script>
  <script type="text/javascript">
  $(document).ready(function(){
		$(".editlink").on("click", function(e){
		  e.preventDefault();
			var dataset = $(this).prev(".datainfo");
			var savebtn = $(this).next(".savebtn");
			var theid   = dataset.attr("id");
			var newid   = theid+"-form";
			var currval = dataset.text();
			
			dataset.empty();
			
			$('<input type="text" name="'+newid+'" id="'+newid+'" value="'+currval+'"class="hlite">').appendTo(dataset);
			
			$(this).css("display", "none");
			savebtn.css("display", "block");
		});
		$(".savebtn").on("click", function(e){
			e.preventDefault();
			var elink   = $(this).prev(".editlink");
			var dataset = elink.prev(".datainfo");
			var newid   = dataset.attr("id");
			
			var cinput  = "#"+newid+"-form";
			var einput  = $(cinput);
			var newval  = einput.attr("value");
			
			$(this).css("display", "none");
			einput.remove();
			dataset.html(newval);
			
			elink.css("display", "block");
		});
	});
  </script>
  <style>
  * { margin: 0; padding: 0; outline: none; }
html { height: 101%; }
body { font-size: 67.5%; background: #fff; font-family: "Calibri", Tahoma, Arial, sans-serif; color: #444; }

img { max-width: 100%; }

a { text-decoration: none; color: #39569d; }
a:hover { text-decoration: underline; }

h1 { font-weight: bold; font-size: 2.15em; line-height: 1.6em; margin-bottom: 10px; color: #676767; }

h2 { display: block; font-weight: bold; line-height: 1.85em; margin-bottom: 5px; }

h3 { font-weight: bold; font-size: 1.5em; line-height: 1.35em; margin-bottom: 2px; color: #777; padding-top: 6px; }

header h1 { font-weight: bold; font-size: 1.7em; line-height: 55px; color: #fff; }
header { display: block; background: #34519b;width : 700px; height: 55px; padding: 0px 515px; }

#wrapper { display: block; padding: 0; max-width: 960px; margin-top: 0px; }

#core { display: block; max-width: 460px; font-family: 'lucida grande', tahoma, verdana, arial, sans-serif; margin-left: 30%; }

.profileinfo { background: #f2f2f2; width: 100%; padding: 4px 10px; border-left: 1px solid #b3b3b3; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; }
.profileinfo h2 { position: relative; left: -250px; }

.gear { position: relative; display: block; margin-bottom: 25px; padding-bottom: 15px; border-bottom: 1px solid #d9d9d9; }

.gear a.editlink { position: absolute; right: 0; top: 13px; }

.datainfo { margin-left: 10px; font-size: 11px; color: #333; }

label { display: inline-block; font-weight: bold; color: #696969; font-size: 12px; width: 100px; }

/** @group form inputs **/
.hlite { background: #e2e8f6; border: 1px solid #bdc7d8; width: 250px; margin-left: -7px; padding: 4px 7px; color: #565656; font-size: 12px; }

/** @group buttons **/
.savebtn { position: absolute; right: 0; top: 13px; padding: 4px 9px; background: #5972a8; font-size: 1.2em; cursor: pointer; border: 1px solid #1a356e; color: #fff; -webkit-box-shadow: inset 0 1px 0 #8a9cc2; -moz-box-shadow: inset 0 1px 0 #8a9cc2; box-shadow: inset 0 1px 0 #8a9cc2; margin-bottom: 5px; margin-top: -5px; display: none;
}
.savebtn:hover { color: #fff; background: #607db7; text-decoration: none; }
.savebtn:active { background: #556790; }

  
  </style>
  
  </head>
<body>
<form action = "UpdateResumeController" method = "post">
<table>
<div id="wrapper">
		<header>
			<h1>Resume</h1>
		</header>
		
		<section id="core">
			<div class="profileinfo">
				<div class="gear">
					<label>Name:</label>
					<span id="fullname" class="datainfo" ><%=name%></span>
					<a href="#" class="editlink">Edit Info</a>
					<button type = "submit" class="savebtn">Save</button>
				</div>
				<div class="gear">
					<label> E-Mail:</label>
					
					<span id="pemail" class="datainfo"><%=email%></span>
					<a href="#" class="editlink">Edit Info</a>
					<a class="savebtn">Save</a>
				</div>
				
				<div class="gear">
					<label>Career Objective:</label>
					<span id="birthday" class="datainfo"><%=careerobjective%></span>
					<a href="#" class="editlink">Edit Info</a>
					<a class="savebtn">Save</a>
				</div>
				
				<div class="gear">
					<label>Profile Summary:</label>
					<span id="citytown" class="datainfo"><%=profilesummary%></span>
					<a href="#" class="editlink">Edit Info</a>
					<a class="savebtn">Save</a>
				</div>
				
				<div class="gear">
					<label>Personal Traits:</label>
					<span id="occupation" class="datainfo"><%=personaltraits%></span>
					<a href="#" class="editlink">Edit Info</a>
					<a class="savebtn">Save</a>
				</div>
				
				<div class="gear">
					<label>Academia:</label>
					<span id="academia" class="datainfo"><%=academia%></span>
					<a href="#" class="editlink">Edit Info</a>
					<a class="savebtn">Save</a>
				</div>
				
				<div class="gear">
					<label>Technical Qualifications:</label>
					<span id="qualification" class="datainfo"><%=technicalqualification%></span>
					<a href="#" class="editlink">Edit Info</a>
					<a class="savebtn">Save</a>
				</div>
				
				<div class="gear">
					<label>Project:</label>
					<span id="project" class="datainfo"><%=project%></span>
					<a href="#" class="editlink">Edit Info</a>
					<a class="savebtn">Save</a>
				</div>
				
				<div class="gear">
					<label>Achievements:</label>
					<span id="achievements" class="datainfo"><%=achievements%></span>
					<a href="#" class="editlink">Edit Info</a>
					<a class="savebtn">Save</a>
				</div>
				
				<div class="gear">
					<label>Personal Details:</label>
					<span id="details" class="datainfo"><%=personaldetails%></span>
					<a href="#" class="editlink">Edit Info</a>
					<a class="savebtn">Save</a>
				</div>
			</div>
		</section>
	</div>

</table>
</form>
</body>
</html>