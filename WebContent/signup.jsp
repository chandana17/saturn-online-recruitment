<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>


<script type="text/javascript"> 
function checkForm(form) {
if(form.user.value == "")
{ 
alert("Error: Username cannot be blank!"); 
form.user.focus(); return false;
} 

if(form.phone.value != "")
{
	re = /[0-9]/; 
	if(!re.test(form.phone.value)) 
	 { 
	 alert("Error: Mobile Number should contain  (0-9)!"); 
	 form.phone.focus(); 
	 return false;
	 }
}


re = /^\w+$/; 
if(!re.test(form.user.value))
{ 
alert("Error: Username must contain only letters, numbers and underscores!");
form.user.focus();
return false;
} 
if(form.password.value != "" && form.password.value == form.rpassword.value)
{ 
 if(form.password.value.length < 6)
 { 
 alert("Error: Password must contain at least six characters!");
 form.password.focus(); 
 return false;
 } 
 if(form.password.value == form.user.value) 
 { 
 alert("Error: Password must be different from Username!");
 form.password.focus();
 return false; 
 } 
 re = /[0-9]/; if(!re.test(form.password.value)) 
 { 
 alert("Error: password must contain at least one number (0-9)!"); 
 form.password.focus(); 
 return false;
 } 
 re = /[a-z]/;
 if(!re.test(form.password.value)) 
 { 
 alert("Error: password must contain at least one lowercase letter (a-z)!"); 
 form.password.focus();
 return false; 
 }
 re = /[A-Z]/;
 if(!re.test(form.password.value)) 
 { 
 alert("Error: password must contain at least one uppercase letter (A-Z)!"); 
 form.password.focus(); 
 return false;
 } 
 re = /[!@#$%^&*()]/;
 if(!re.test(form.password.value)) 
 { 
 alert("Error: password must contain at least one Special Character"); 
 form.password.focus(); 
 return false;
 } 
 } 
 else 
 { 
 alert("Error: Please check that you've entered and confirmed your password!"); 
 form.password.focus(); 
 return false; 
 } 
 alert("You entered a valid password");
 return true;
 } 
     
 </script>
  





<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Sign Up</title>
<style>
.jumbo {
    position: relative;
    background-size: cover;
    height : 80px;
    width : 1140px;
    overflow: hidden;
    background-color:#126180;
}
.jumbotron{
   position : absolute;
   top : 130px;
   left-margin : 470px;
   width : 600px;
   height :460px;
}


.con{
position : absolute;
top : 900px;
width :1287px;
align : center;
}


</style>
</head>

<body class = "body">
<div class="container">
  <div class="jumbo">
   <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 pull-right "> <img vspace="20" class="jumbowidth"  src="./images/talentsprintimg.jpg" width = 150 height = 50> </div>
    <h2 align = center style = "color : white"><b><i>Online Recruitment System</i></b></h2>                        
  </div>
  </div>
  
<div class="col-sm-4"></div>
<div class="header">
				 <!-- <div class="container">	
				   <div class="logo">
					<a href="index.html"><img src="images/talentsprintimg.jpg" width = 150 height = 50 alt=""/></a>
					
				</div>
				 -->
					<div>
   <ul class="nav navbar-nav navbar-left">
        <li class="dropdown"><a  href="Mainpage.jsp"><span class="glyphicon glyphicon-home"></span><b>Home</b></a>
        </li>
   </ul>
    </div>
<!-- <div class="container">
<div class="col-sm-4"></div>
<div class="jumbo"> 


    <h2 align = center style = "color : white"><b><i>Online Recruitment System</i></b></h2>            
</div>-->
<div class="container">
<div class="jumbotron" style="margin-left: 270px">
 <form action = "SignupController" name="example_form" id = "myform"  onsubmit="return checkForm(this);" >
<table class="table" >
<thead>
<tr><th colspan="2" ><h2 align="center"><b><i>Registration Form</i></b></h2></th></tr></thead>
<tr><td><b>Username:<span class="red">*</span></b></td><td><input type = text name = user required></td></tr>
<tr><td><b>Email:<span class="red">*</span></b></td><td><input type = text name = email required></td></tr>
<tr><td><b>Password:<span class="red">*</span></b></td><td><input type = password name = password required></td></tr>
<tr><td><b>Re-Type Password:<span class="red">*</span></b></td><td><input type = password name = rpassword required></td></tr>
<tr><td><b>Mobile Num:<span class="red">*</span></b></td><td><input type = text name = phone required></td></tr>
<tr><td><b>Gender
:</b></td><td><select name = gender required> 
	<option value="Female">Female</option>
    <option value="Male">Male</option>
<tr><td><b>Address:<span class="red">*</span></b></td><td><input type = text name = address required></td></tr>
<tr><td align="center"><input type = submit value = Submit></td></tr>
</table>
</form>
</div>
</div>
</div>

<div class="con">
    <div class="panel panel-default">
    <div class="panel-body">Copyright © TalentSprint, 2015. All Rights Reserved.</div>
  </div>
</div>


</body>
</html>