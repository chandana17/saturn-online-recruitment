


<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Vacancies</title>
<style>
.jumbo {
    position: relative;
    background-size: cover;
    height : 80px;
    width : 1140px;
    overflow: hidden;
    background-color:#126180;
}
.con{
position : absolute;
top : 900px;
width :1287px;
align : center;
}

</style>
</head>
<body class = "body">
<div class="container">
<div class="col-sm-4"></div>
<div class="jumbo">
    <h2 align = center style = "color : white"><b><i>Online Recruitment System</i></b></h2>            
</div>
<a href = "userprofile2.jsp">Profile</a><a href = "#">>></a><a href = "profilevacancies.jsp">Vacancies</a><a href = "#">>></a>
<a href = "profilevacancy2.jsp">Java Faculty in Software Engineering </a>
<div class = "case1">
<h3><font color = "olive"><i><b>Java Faculty in Software Engineering</b></i></font></h3>
<div>Number of People Required : 4
</div><br>

<div>Qualification : M.C.A/M.Tech (Computers)
</div><br>

<div>Responsibilities/Job Description:
</div><br>
<div>Training the students on 
<ul>
    <li>Essentials of SQL</li>
    <li>Core Java</li>
    <li>Advance Java </li>
</ul>
</div> <br>
<div>Years of experience required : 5+ years of experience in teaching or similar role</div><br>

<div>Location : Hyderabad</div><br>

<div>Skill sets required:
<ul>
<li>Requisite knowledge, skills and ability to work in JAVA</li>
<li>Good communication skills</li>
<li>Passion for teaching students</li>
</ul>
</div>
<div>
<a href = "resume.jsp"><h4>Apply for the job</h4></a>
</div>

</div>
</div>
<div class="con">
    <div class="panel panel-default">
    <div class="panel-body">Copyright © TalentSprint, 2015. All Rights Reserved.</div>
  </div>
</div>

</body>
</html>

