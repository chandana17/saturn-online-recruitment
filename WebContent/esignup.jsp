<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Sign Up</title>
<style>
.jumbo {
    position: relative;
    background-size: cover;
    height : 80px;
    width : 1140px;
    overflow: hidden;
    background-color:#126180;
}
.jumbotron{
   position : absolute;
   top : 130px;
   left-margin : 470px;
   width : 600px;
   height :400px;
}

</style>
</head>

<body class = "body">
<div class="container">
<div class="col-sm-4"></div>
<div class="jumbo">
    <h2 align = center style = "color : white"><b><i>Online Recruitment System</i></b></h2>            
</div>
<div class="container">
<div class="jumbotron" style="margin-left: 270px">
 <form action = "ESignupController" >
<table class="table" >
<thead>
<tr><th colspan="2" ><h2 align="center"><b><i>Registration Form</i></b></h2></th></tr></thead>
<tr><td><b>Username:<span class="red">*</span></b></td><td><input type = text name = user required></td></tr>
<tr><td><b>Email:<span class="red">*</span></b></td><td><input type = text name = email required></td></tr>
<tr><td><b>Password:<span class="red">*</span></b></td><td><input type = password name = password required></td></tr>
<tr><td><b>Designation:<span class="red">*</span></b></td><td><input type = text name = designation required></td></tr>
<tr><td align="center"><input type = submit value = Submit></td></tr>
</table>
</form>
</div>
</div>
</div>
</body>
</html>