package com.bvrit.ors.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class ConnectionDAO {
	//Logger log = Logger.getLogger(ConnectionDAO.class);
	Logger log = Logger.getRootLogger();
	Connection conn = null;
	//String URL,PASSWORD,USER;
	public Connection getConnection() throws ClassNotFoundException, NamingException, SQLException
	{
		/*String url = "jdbc:mysql://localhost:3306/shopaholics";
		String username = "root";
		String password = "root";*/
		
	        /*try {
	            // specify the location of the .property file here
	            File file = new File("/home/ubuntu/Desktop/eclipse/Online recruitment system/ors(4)/config.properties");
	            FileInputStream fileInput = new FileInputStream(file);
	            Properties properties = new Properties();
	            properties.load(fileInput);
	            fileInput.close();

	            Enumeration enuKeys = properties.keys();
	            while (enuKeys.hasMoreElements()) {
	                String key = (String) enuKeys.nextElement();
	                String value = properties.getProperty(key);
	                System.out.println(key + ": " + value);
	            }
	            URL = properties.getProperty("url");
	            USER = properties.getProperty("username");
	            PASSWORD = properties.getProperty("password");
	        } catch (FileNotFoundException e) {
	            e.printStackTrace();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }*/
		try {
			//load Driver
			//Class.forName("com.mysql.jdbc.Driver");
			
			Context initContext = new InitialContext();
			Context envContext = (Context) initContext.lookup("java:comp/env");
			DataSource ds = (DataSource) envContext.lookup("jdbc/ors");
			Connection conn = ds.getConnection();
			log.info("driver loaded");
			//create connection
			//conn = DriverManager.getConnection(URL, USER, PASSWORD);
			log.info("driver loaded");
			return conn;
		} /*catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		catch(NamingException c){
			System.out.println("Driver not found");
	}
		return null;
	}
}





/*package com.bvrit.ors.dao;


import java.io.IOException;

import java.sql.Connection;

import java.sql.DriverManager;

import java.sql.SQLException;

import java.util.Properties;

public class ConnectionDAO {

Connection conn;

public Connection getConnection() throws ClassNotFoundException, IOException

{


 String url = "jdbc:mysql://localhost:3306/ors";

String username = "root";

String password = "123456";

	
Connection conn = null;

try {

//load Driver

Class.forName("com.mysql.jdbc.Driver");

System.out.println("driver loaded");

//create connection

conn = DriverManager.getConnection(url, username, password);

System.out.println("successfully connected");

} catch (ClassNotFoundException e) {

// TODO Auto-generated catch block

e.printStackTrace();

} catch (SQLException e) {

// TODO Auto-generated catch block

e.printStackTrace();

}

return conn;

}

}*/