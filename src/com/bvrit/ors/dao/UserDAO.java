package com.bvrit.ors.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.naming.NamingException;

import com.bvrit.ors.beans.UserBean;

public class UserDAO {
	 Connection conn;
	  PreparedStatement st;
	  ConnectionDAO cd;
	  public UserDAO() throws ClassNotFoundException, NamingException, SQLException{
		  cd = new ConnectionDAO(); 
		  conn = cd.getConnection();
	  }
	public int createUser(UserBean ubean) 	
	{
		     String user = ubean.getUser();
		     String email = ubean.getEmail();
		     String password = ubean.getPassword();
		     String phone = ubean.getPhone();
		     String gender = ubean.getGender();
		     String address = ubean.getAddress();
			 String query = "insert into usersignup values(?,?,?,?,?,?)";
			 int result = 0;
			 try {
			 st = conn.prepareStatement(query);
			 st.setString(1, user);
			 st.setString(2, email);
			 st.setString(3, password);
			 st.setString(4, phone);
			 st.setString(5, gender);
			 st.setString(6, address);
			 System.out.println(st);
		     result = st.executeUpdate();
			 if(result >= 1) {
			    System.out.println("Record inserted");
			 }
		     else {
				System.out.println("Record is not inserted");
				} 
			 }
			 catch (SQLException e) {					
					e.printStackTrace();
				}
			finally {
				try {
					if (conn != null){
						conn.close();
					}
					if (st != null) {
					st.close();
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			 return result;
	    }
    public boolean validateUser(UserBean ubean) {
    	// String user = ubean.getUser();
    	//verify  whether the user who is logged in is valid user
    	String email = ubean.getEmail();
	    String password = ubean.getPassword();
    	boolean result = false;
    	ResultSet rs;
    	String query = "SELECT * from usersignup WHERE email='" + email + "'and password='"+ password +"'";   	
    	try {	    		   		
   		 st = conn.prepareStatement(query);	   		
   		 rs = st.executeQuery();
   		if(rs.absolute(1)) {
   			result = true;
   		}
    	}
    	catch (SQLException e) {			
			e.printStackTrace();
		}
    	finally {
			try {
				if (conn != null){
					conn.close();
				}
				if (st != null) {
				st.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
    	return result;		
    }
    
    public int update(String password, String npassword) throws SQLException, ClassNotFoundException {
 	   // String user = cbean.getUser();
 		String query = "update usersignup set password ='"+npassword+"'where password='"+password+"'";
 		System.out.println(query);
 		Statement st = (Statement) conn.createStatement();
 		// st = conn.createStatement();
 	     int result = st.executeUpdate(query);
          return result;
 	}

}
