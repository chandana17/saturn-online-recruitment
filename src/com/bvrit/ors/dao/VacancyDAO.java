package com.bvrit.ors.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.naming.NamingException;

import com.bvrit.ors.beans.CreateVacancyBean;

public class VacancyDAO {
private int noOfRecords;
	public int getNoOfRecords(){
		return noOfRecords;
	}
    Statement st=null;
    Connection conn = null;
    PreparedStatement pst;

    public VacancyDAO() throws SQLException, ClassNotFoundException, NamingException{
         ConnectionDAO postDAO = new ConnectionDAO();
      
         conn = postDAO.getConnection();
         st = conn.createStatement();
         System.out.println("connected to database");
    }

    public int createvacancy(CreateVacancyBean pbean) throws SQLException{
    
        String query = "insert into vacancy values(?,?,?,?,?,?,?)";
        int result=0;
         pst =conn.prepareStatement(query);
         //System.out.println(query);
         //pst =conn.prepareStatement(query);
         pst.setString(1, pbean.getJobtitle());
         pst.setString(2, pbean.getVacancies());
         pst.setString(3, pbean.getJobdescription());
         pst.setString(4, pbean.getWorkexperience());
         pst.setString(5, pbean.getLocation());
         pst.setString(6, pbean.getQualification());
         pst.setString(7, pbean.getStatus());
         result = pst.executeUpdate();
         System.out.println("in dao");
        return result;
       
    }
   
    public List<CreateVacancyBean> viewvacancies(int offset , int noOfRecords)throws SQLException{
    	//String query= "SELECT * from ";
	   String query = "SELECT SQL_CALC_FOUND_ROWS * from vacancy limit "+ offset + ", " + noOfRecords;
	   System.out.println(query);
	   List<CreateVacancyBean> pbeanList = new ArrayList<CreateVacancyBean>();
       pst = conn.prepareStatement(query);
       ResultSet rs = pst.executeQuery();
       CreateVacancyBean pbean;
       while(rs.next()){
    	   
           pbean = new CreateVacancyBean(rs.getString("jobtitle"), rs.getString("vacancies"),rs.getString("jobdescription"),rs.getString("workexperience"),rs.getString("location"),rs.getString("qualification"),rs.getString("status"));
           pbeanList.add(pbean);
       }
       
       rs.close();
		
	   rs = st.executeQuery("SELECT FOUND_ROWS()");
		
		if(rs.next())
			this.noOfRecords = rs.getInt(1);
       return pbeanList;
   }
   

    public List<CreateVacancyBean> listvacancies()throws SQLException{
		String query = "SELECT * from vacancy";
      List<CreateVacancyBean> pbeanList = new ArrayList<CreateVacancyBean>();
      pst = conn.prepareStatement(query);
      ResultSet rs = pst.executeQuery(query);
      CreateVacancyBean pbean;
      while(rs.next()){
   	   System.out.print("I AM IN WHILE");
          pbean = new CreateVacancyBean(rs.getString("jobtitle"), rs.getString("vacancies"),rs.getString("jobdescription"),rs.getString("workexperience"),rs.getString("location"),rs.getString("qualification"),rs.getString("status"));
        		  pbeanList.add(pbean);
      }
      return pbeanList;
  }
    
}