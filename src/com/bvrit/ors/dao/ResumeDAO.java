package com.bvrit.ors.dao;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;






import javax.naming.NamingException;

//import com.bvrit.ors.beans.QuestionBean;
import com.bvrit.ors.beans.ResumeBean;

	public class ResumeDAO {
		PreparedStatement st;
		Connection conn;
		int result;
		ConnectionDAO cd;
		public ResumeDAO() throws ClassNotFoundException, IOException, NamingException, SQLException {
			super();
			cd = new ConnectionDAO();
			conn = (Connection) cd.getConnection();
		}
		public int addresume(ResumeBean qbeans) throws SQLException{
			String query = "INSERT INTO resume values(?,?,?,?,?,?,?,?,?,?)";
			st = conn.prepareStatement(query);
			String name = qbeans.getName();
			String email = qbeans.getEmail();
			String careerobjective = qbeans.getCareerobjective();
			String profilesummary = qbeans.getProfilesummary();
			String personaltraits = qbeans.getPersonaltraits();
			String academia = qbeans.getAcademia();
			String technicalqualification = qbeans.getTechnicalqualification();
			String project = qbeans.getProject();
			String achievements = qbeans.getAchievements();
			String personaldetails = qbeans.getPersonaldetails();
			
			st.setString(1,name);
			st.setString(2,email);
			st.setString(3,careerobjective);
			st.setString(4,profilesummary);
			st.setString(5,personaltraits);
			st.setString(6,academia);
			st.setString(7,technicalqualification);
			st.setString(8,project);
			st.setString(9,achievements);
			st.setString(10,personaldetails);
			System.out.println(query);
			result = st.executeUpdate();
			return result;
		}
		public List<ResumeBean> listResume() throws SQLException{
			String query = "select * from resume ";
			System.out.println(query);
			List<ResumeBean> qbeanlist = new ArrayList<ResumeBean>();
			st = conn.prepareStatement(query);
			ResultSet rs = st.executeQuery();
			ResumeBean bean;
			while(rs.next()){
				String name = rs.getString("name");
				String Email = rs.getString("email");
				String careerobjective = rs.getString("careerobjective");
				String profilesummary = rs.getString("profilesummary");
				String personaltraits = rs.getString("personaltraits");
				String academia = rs.getString("academia");
				String technicalqualification = rs.getString("technicalqualifications");
				String project = rs.getString("project");
				String achievements = rs.getString("achievements");
				String personaldetails = rs.getString("personaldetails");
				bean = new ResumeBean(name, Email, careerobjective, profilesummary, personaltraits, academia, technicalqualification, project, achievements, personaldetails);
				qbeanlist.add(bean);
			}
			return qbeanlist;
		}
		
		public int updateresume(ResumeBean qbeans, String email) throws SQLException
		{
			String name = qbeans.getName();
			
			String Email = qbeans.getEmail();
			String careerobjective = qbeans.getCareerobjective();
			String profilesummary = qbeans.getProfilesummary();
			String personaltraits = qbeans.getPersonaltraits();
			String academia = qbeans.getAcademia();
			String technicalqualification = qbeans.getTechnicalqualification();
			String project = qbeans.getProject();
			String achievements = qbeans.getAchievements();
			String personaldetails = qbeans.getPersonaldetails();
			 
			String query = "update resume set name ='"+name+"',email='"+Email+"',careerobjective='"+careerobjective+"',profilesummary='"+profilesummary+"',personaltraits='"+personaltraits+"',academia='"+academia+"',technicalqualifications='"+technicalqualification+"',project='"+project+"',achievements='"+achievements+"',personaldetails='"+personaldetails+"' where email = '"+email+"'";
			st = conn.prepareStatement(query);
			return result;
			
		
		
			
			
		}
			}

