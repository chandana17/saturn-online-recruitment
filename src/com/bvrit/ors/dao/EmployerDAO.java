package com.bvrit.ors.dao;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.naming.NamingException;

import com.bvrit.ors.beans.EmployerBean;

public class EmployerDAO {
	 Connection conn;
	  PreparedStatement st;
	  ConnectionDAO cd;
	  public EmployerDAO() throws ClassNotFoundException, IOException, NamingException, SQLException{
		  cd = new ConnectionDAO(); 
		  conn = cd.getConnection();
	  }
	public int createEmployer(EmployerBean ebean) 	
	{
		     String user = ebean.getEmpname();
		     String email = ebean.getEmail();
		     String password = ebean.getPassword();
		     String designation = ebean.getDesignation();
			 String query = "insert into empsignup values(?,?,?,?)";
			 int result = 0;
			 try {
			 st = conn.prepareStatement(query);
			 st.setString(1, user);
			 st.setString(2, email);
			 st.setString(3, password);
			 st.setString(4, designation);
			 System.out.println(st);
		     result = st.executeUpdate();
			 if(result >= 1) {
			    System.out.println("Record inserted");
			 }
		     else {
				System.out.println("Record is not inserted");
				} 
			 }
			 catch (SQLException e) {					
					e.printStackTrace();
				}
			finally {
				try {
					if (conn != null){
						conn.close();
					}
					if (st != null) {
					st.close();
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			 return result;
	    }
   public boolean validateEmployer(EmployerBean ebean) {
   	// String user = ebean.getEmpname();
   	//verify  whether the user who is logged in is valid user
   	    String email = ebean.getEmail();
	    String password = ebean.getPassword();
   	boolean result = false;
   	ResultSet rs;
   	String query = "SELECT * from empsignup WHERE email='" + email + "' and password='"+ password +"'";   	
   	try {	    		   		
  		 st = conn.prepareStatement(query);	   		
  		 rs = st.executeQuery();
  		if(rs.absolute(1)) {
  			result = true;
  		}
   	}
   	catch (SQLException e) {			
			e.printStackTrace();
		}
   	finally {
			try {
				if (conn != null){
					conn.close();
				}
				if (st != null) {
				st.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
   	return result;		
   }

}
