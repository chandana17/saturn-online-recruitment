package com.bvrit.ors.dao;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.naming.NamingException;

import com.bvrit.ors.beans.*;

public class QuestionDAO {
	
	private int noOfRecords;
	
	public int getNoOfRecords(){
		return noOfRecords;
	}
	
	PreparedStatement st;
	Connection conn;
	int result;
	ConnectionDAO cd;
	public QuestionDAO() throws ClassNotFoundException, IOException, NamingException, SQLException {
		super();
		cd = new ConnectionDAO();
		conn = (Connection) cd.getConnection();
	}
	public int addQuestion(QuestionBeans qbeans) throws SQLException{
		String query = "INSERT INTO questionbank values(?,?,?,?,?,?,?)";
		st = conn.prepareStatement(query);
		String number = qbeans.getNumber();
		String name = qbeans.getName();
		String option1 = qbeans.getOption1();
		String option2 = qbeans.getOption2();
		String option3 = qbeans.getOption3();
		String option4 = qbeans.getOption4();
		String correct = qbeans.getCorrect();
		
		st.setString(1, number);
		st.setString(2,name);
		st.setString(3,option1);
		st.setString(4,option2);
		st.setString(5,option3);
		st.setString(6,option4);
		st.setString(7,correct);
		System.out.println(query);
		result = st.executeUpdate();
		return result;
	}
	public List<QuestionBeans> listQuestions(int offset, int noOfRecords) throws SQLException{
			
		String query = "select SQL_CALC_FOUND_ROWS * from questionbank limit "
				 + offset + ", " + noOfRecords;
		//String query = "select * from questionbank";
		System.out.println(query);
		List<QuestionBeans> qbeanlist = new ArrayList<QuestionBeans>();
		st = conn.prepareStatement(query);
		ResultSet rs = st.executeQuery();
		QuestionBeans bean;
		while(rs.next()){
			String number = rs.getString(1);
			String name = rs.getString(2);
			String option1 = rs.getString(3);
			String option2 = rs.getString(4);
			String option3 = rs.getString(5);
			String option4 = rs.getString(6);
			String correct = rs.getString(7);
			bean = new QuestionBeans(number, name, option1, option2, option3, option4, correct);
			qbeanlist.add(bean);
		}
		
		rs.close();
		
		rs = st.executeQuery("SELECT FOUND_ROWS()");
		if(rs.next())
			this.noOfRecords = rs.getInt(1);
		
		return qbeanlist;
	}

	//update method
	
		public int update(QuestionBeans qbean) throws SQLException
	    {
			String number = qbean.getNumber();
			String name = qbean.getName();
			String option1 = qbean.getOption1();
			String option2 = qbean.getOption2();
			String option3 = qbean.getOption3();
			String option4 = qbean.getOption4();
			String correct = qbean.getCorrect();
			String query = "update questionbank set quest ='"+name+"',QA='"+option1+"',QB='"+option2+"',QC='"+option3+"',QD='"+option4+"',correctAns='"+correct+"' where number='"+number+"'";
			st = conn.prepareStatement(query);
			int result = st.executeUpdate();
			return result;

	    }
		
		//delete method()

		public void delete(String number) throws SQLException
	    {
			String query = "delete from questionbank where number='"+number+"'";
			st = conn.prepareStatement(query);
			int result = st.executeUpdate();
			System.out.println(result);
	    }
	
}
