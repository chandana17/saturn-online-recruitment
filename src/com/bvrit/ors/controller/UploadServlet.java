package com.bvrit.ors.controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bvrit.ors.beans.QuestionBeans;
import com.bvrit.ors.dao.QuestionDAO;
import com.bvrit.ors.dao.UserDAO;
public class UploadServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public UploadServlet() {
        super();

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doProcess(request,response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doProcess(request,response);
	}
	
	 protected void doProcess(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{	
		 try {
			 
			 String oldpassword = request.getParameter("password");
			 String npassword = request.getParameter("npassword");
			 String rpassword = request.getParameter("rpassword");
			 //reading email from session 
			// HttpSession hs = request.getSession();
			// String email = (String) hs.getAttribute("email");
			 
			 int result = 0;
			 
			 //creating UserDAO object			 
			 UserDAO qda = new UserDAO();
			 //calling update method 
			 if(npassword.equals(rpassword))
			 	result = qda.update(oldpassword, npassword);
			  
			 //redirecting response
			 if(result == 1)
			 {
				 response.sendRedirect("userprofile.jsp?message=updated successfully");
			 }
			 else
			 {
				 response.sendRedirect("ChangePassword.jsp?message=your password has not been changed");
			 }
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}catch (SQLException e){
			e.printStackTrace();
		} catch (NamingException e) {
			e.printStackTrace();
		}
	 }

}

















/*package com.bvrit.ors.controller;

import java.io.IOException; 

import javax.servlet.ServletException; 
import javax.servlet.http.HttpServlet; 
import javax.servlet.http.HttpServletRequest; 
import javax.servlet.http.HttpServletResponse; 
 
import java.io.InputStream; 
import java.sql.Connection; 
import java.sql.DriverManager; 
import java.sql.PreparedStatement; 
import java.sql.SQLException; 
  
 
 
 
 
 
import javax.servlet.annotation.MultipartConfig; 
import javax.servlet.annotation.WebServlet; 
import javax.servlet.http.Part; 
 
*//** 
 * Servlet implementation class UploadServlet 
 *//* 
@WebServlet("/uploadServlet") 
@MultipartConfig(maxFileSize = 16177215)  
public class UploadServlet extends HttpServlet { 
    String url ="jdbc:mysql://localhost:3306/ors"; 
    //String url ="jdbc:postgresql://localhost:5432/sample"; 
    String user ="root"; 
    //String user ="postgres"; 
    String password ="123456"; 
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException { 
        // TODO Auto-generated method stub 
        doProcess(request, response); 
         
    } 
 
     
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException { 
        // TODO Auto-generated method stub 
        doProcess(request, response); 
    } 
    protected void doProcess(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException { 
        // gets values of text fields 
        String firstName = request.getParameter("firstName"); 
        System.out.println("before"); 
        String lastname = request.getParameter("lastname"); 
        //System.out.println("after"); 
        String email = request.getParameter("email"); 
        String university = request.getParameter("university"); 
        String percentage = request.getParameter("percentage"); 
        String experience = request.getParameter("experience"); 
        String applyforpost = request.getParameter("applyforpost"); 
        String img = request.getParameter("photo"); 
        InputStream inputStream = null; // input stream of the upload file 
        //System.out.println(firstName); 
        //System.out.println(img_id); 
         //System.out.println(img); 
        // obtains the upload file part in this multipart request 
        Part filePart = request.getPart("photo"); 
        System.out.println(filePart); 
        if (filePart != null) { 
            // prints out some information for debugging 
            System.out.println(filePart.getName()); 
            System.out.println(filePart.getSize()); 
            System.out.println(filePart.getContentType()); 
              
            // obtains input stream of the upload file 
            inputStream = filePart.getInputStream(); 
        } 
          
        Connection conn = null; // connection to the database 
        String message = null;  // message will be sent back to client 
         
         
        try { 
            // connects to the database 
            Class.forName("com.mysql.jdbc.Driver"); 
            //Class.forName("org.postgresql.Driver"); 
            System.out.println("mysql driver is loaded"); 
            conn = DriverManager.getConnection(url, user, password); 
  
            // constructs SQL statement 
            String sql = "insert into contacts values (?, ?, ?, ?, ?, ?, ?, ?)"; 
            PreparedStatement statement = conn.prepareStatement(sql); 
            statement.setString(1, firstName); 
            statement.setString(2, lastname); 
            statement.setString(3, email); 
            statement.setString(4, university); 
            statement.setString(5, percentage); 
            statement.setString(6, experience); 
            statement.setString(7, applyforpost); 
            //statement.setString(2, lastname); 
              
            if (inputStream != null) { 
                // fetches input stream of the upload file for the blob column 
                System.out.println(inputStream); 
                statement.setBlob(8, inputStream); 
            } 
             
            // sends the statement to the database server 
            int row = statement.executeUpdate(); 
            if (row > 0) { 
                message = "File uploaded Successfully"; 
            } 
        } catch (SQLException ex) { 
            message = "ERROR: " + ex.getMessage(); 
            ex.printStackTrace(); 
        } catch (ClassNotFoundException e) { 
            // TODO Auto-generated catch block 
            e.printStackTrace(); 
        } finally { 
            if (conn != null) { 
                // closes the database connection 
                try { 
                    conn.close(); 
                } catch (SQLException ex) { 
                    ex.printStackTrace(); 
                } 
            } 
            // sets the message in request scope 
            request.setAttribute("Message", message); 
              
            // forwards to the message page 
            getServletContext().getRequestDispatcher("/Message.jsp").forward(request, response); 
        } 
    } 
    } */