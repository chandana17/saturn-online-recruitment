package com.bvrit.ors.controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bvrit.ors.beans.UserBean;
import com.bvrit.ors.dao.UserDAO;

/**
 * Servlet implementation class SignupController
 */
public class SignupController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
 	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
 		try {
			doProcess(request, response);
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			doProcess(request, response);
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	protected void doProcess(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, NamingException, SQLException {
		try {
		//read user and password	
		String user = request.getParameter("user");
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		String phone = request.getParameter("phone");
		String gender = request.getParameter("gender");
		String address = request.getParameter("address");
		//creating ContactBean object with parameterized constructor
		UserBean ubean = new UserBean(user, email, password, phone, gender, address);//creating UserDAO object
		UserDAO ud = new UserDAO();
		//calling createUser() method
		int result = ud.createUser(ubean);
		if(result > 0)
		{
			//get http Session reference
			//HttpSession hs = request.getSession();
			//add name as session attribute
			//hs.setAttribute("sunm", user);
			//hs.setAttribute("semail", email);
			response.sendRedirect("Mainpage.jsp");									
			
		}
		else
		{
			response.sendRedirect("signup.jsp?username=" + user);
			
		}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}		
	}

}
