package com.bvrit.ors.controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
//import javax.servlet.http.HttpSession;


import com.bvrit.ors.dao.QuestionDAO;
import com.bvrit.ors.beans.QuestionBeans;

/**
 * Servlet implementation class AddQuestionController
 */
public class AddQuestionController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try {
			doProcess(request, response);
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try {
			doProcess(request, response);
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	protected void doProcess(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, NamingException {
		// TODO Auto-generated method stub
		try {
	
			String number = request.getParameter("number");
			String name = request.getParameter("question");
			String option1 = request.getParameter("option1");
			String option2 = request.getParameter("option2");
			String option3 = request.getParameter("option3");
			String option4 = request.getParameter("option4");
			String correct = request.getParameter("answer");
			/*HttpSession hs = request.getSession();
			String questionno = (String) hs.getAttribute("no");
			System.out.println(questionno);*/
			int result;
			QuestionBeans qbean = new QuestionBeans(number, name, option1, option2, option3, option4, correct);
			QuestionDAO qda = new QuestionDAO();
			result = qda.addQuestion(qbean);
			//System.out.println(result);
			if(result >= 1){
				response.sendRedirect("ListQuestion.jsp");
			}
			else{
				response.sendRedirect("questions.jsp");
			}
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	}

