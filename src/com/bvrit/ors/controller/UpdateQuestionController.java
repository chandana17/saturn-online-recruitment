package com.bvrit.ors.controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bvrit.ors.beans.QuestionBeans;
import com.bvrit.ors.dao.QuestionDAO;

public class UpdateQuestionController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public UpdateQuestionController() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			doProcess(request,response);
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			doProcess(request,response);
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	 protected void doProcess(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, NamingException{	
		 try {
			// read qno, question, options and correct option from request
			 String number = request.getParameter("number");
			 String name = request.getParameter("question");
			 String option1 = request.getParameter("option1");
			 String option2 = request.getParameter("option2");
			 String option3 = request.getParameter("option3");
			 String option4 = request.getParameter("option4");
			 String correct = request.getParameter("answer");
			 //reading email from session 
			 HttpSession hs = request.getSession();
			 String email = (String) hs.getAttribute("email");
			 //creating QuestionBeans object with parameterized constructor
			 QuestionBeans qbean = new QuestionBeans(number, name, option1, option2, option3, option4, correct);
			 //creating QuestionDAO object
			 QuestionDAO qda = new QuestionDAO();
			 //calling update method 
			 int result = qda.update(qbean);
			 //redirecting response
			 if(result == 1)
			 {
				 response.sendRedirect("ListQuestion.jsp?message=updated successfully");
			 }
			 else
			 {
				 response.sendRedirect("updatequestion.jsp?number="+number);
			 }
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}catch (SQLException e){
			e.printStackTrace();
		}
	 }
	
}
