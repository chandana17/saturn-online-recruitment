package com.bvrit.ors.controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bvrit.ors.dao.VacancyDAO;
import com.bvrit.ors.beans.CreateVacancyBean;


public class VacancyController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			try {
				doProcess(request, response);
			} catch (NamingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			doProcess(request, response);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	protected void doProcess(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ClassNotFoundException, NamingException {
		int result=0;
		HttpSession hs = request.getSession();
		String jobtitle= request.getParameter("jobtitle");
		String vacancies = request.getParameter("vacancies");
		String jobdescription = request.getParameter("jobdescription");
        String workexperience = request.getParameter("workexperience");
        String location= request.getParameter("location");
    	String qualification = request.getParameter("qualification");
		String status= request.getParameter("status");
	
		System.out.println("before try");
		try {
			VacancyDAO pdao = new VacancyDAO();
			CreateVacancyBean pBean = new CreateVacancyBean(jobtitle, vacancies, jobdescription, workexperience,location, qualification, status);
			 result = pdao.createvacancy(pBean);
			 System.out.println("result");
			 
			 if(result == 1){
					response.sendRedirect("Viewvacancies.jsp");
				}		
				else{
				response.sendRedirect("CreateVacancy.jsp");
			}
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
}

