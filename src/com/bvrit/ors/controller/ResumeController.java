package com.bvrit.ors.controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bvrit.ors.beans.QuestionBeans;
import com.bvrit.ors.beans.ResumeBean;
import com.bvrit.ors.dao.QuestionDAO;
import com.bvrit.ors.dao.ResumeDAO;

/**
 * Servlet implementation class ResumeController
 */
public class ResumeController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			doProcess(request, response);
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			doProcess(request, response);
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	protected void doProcess(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, NamingException {
		try {
			String name = request.getParameter("name");
			String email = request.getParameter("email");
			String careerobjective = request.getParameter("careerobjective");
			String profilesummary = request.getParameter("profilesummary");
			String personaltraits = request.getParameter("personaltraits");
			String academia = request.getParameter("academia");
			String technicalqualification = request.getParameter("technicalqualification");
			String project = request.getParameter("project");
			String achievements = request.getParameter("achievements");
			String personaldetails = request.getParameter("personaldetails");
			/*HttpSession hs = request.getSession();
			String questionno = (String) hs.getAttribute("no");
			System.out.println(questionno);*/
			int result;
			ResumeBean qbean = new ResumeBean(name, email, careerobjective, profilesummary, personaltraits, academia, technicalqualification, project, achievements, personaldetails);
			ResumeDAO qda = new ResumeDAO();
			result = qda.addresume(qbean);
			System.out.println(result);
			if(result >= 1){
				response.sendRedirect("userprofile.jsp");
			}
			else{
				response.sendRedirect("resume.jsp");
			}
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
}
