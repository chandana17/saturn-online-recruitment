package com.bvrit.ors.beans;

public class CreateVacancyBean {

	String jobtitle;
	String vacancies;
	String jobdescription;
	String workexperience;
	String location;
	String qualification;
	String status;
	public CreateVacancyBean(String jobtitle, String vacancies,
			String jobdescription, String workexperience, String location,
			String qualification, String status) {
		super();
		this.jobtitle = jobtitle;
		this.vacancies = vacancies;
		this.jobdescription = jobdescription;
		this.workexperience = workexperience;
		this.location = location;
		this.qualification = qualification;
		this.status = status;
	}
	public CreateVacancyBean() {
		super();
		// TODO Auto-generated constructor stub
	}
	public String getJobtitle() {
		return jobtitle;
	}
	public void setJobtitle(String jobtitle) {
		this.jobtitle = jobtitle;
	}
	public String getVacancies() {
		return vacancies;
	}
	public void setVacancies(String vacancies) {
		this.vacancies = vacancies;
	}
	public String getJobdescription() {
		return jobdescription;
	}
	public void setJobdescription(String jobdescription) {
		this.jobdescription = jobdescription;
	}
	public String getWorkexperience() {
		return workexperience;
	}
	public void setWorkexperience(String workexperience) {
		this.workexperience = workexperience;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getQualification() {
		return qualification;
	}
	public void setQualification(String qualification) {
		this.qualification = qualification;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

	
	
}

