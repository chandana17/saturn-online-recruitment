package com.bvrit.ors.beans;

public class ResumeBean {
	String name;
	String email;
	String careerobjective;
	String profilesummary;
	String personaltraits;
	String academia;
	String technicalqualification;
	String project;
	String achievements;
	String personaldetails;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getCareerobjective() {
		return careerobjective;
	}
	public void setCareerobjective(String careerobjective) {
		this.careerobjective = careerobjective;
	}
	public String getProfilesummary() {
		return profilesummary;
	}
	public void setProfilesummary(String profilesummary) {
		this.profilesummary = profilesummary;
	}
	public String getPersonaltraits() {
		return personaltraits;
	}
	public void setPersonaltraits(String personaltraits) {
		this.personaltraits = personaltraits;
	}
	public String getAcademia() {
		return academia;
	}
	public void setAcademia(String academia) {
		this.academia = academia;
	}
	public String getTechnicalqualification() {
		return technicalqualification;
	}
	public void setTechnicalqualification(String technicalqualification) {
		this.technicalqualification = technicalqualification;
	}
	public String getProject() {
		return project;
	}
	public void setProject(String project) {
		this.project = project;
	}
	public String getAchievements() {
		return achievements;
	}
	public void setAchievements(String achievements) {
		this.achievements = achievements;
	}
	public String getPersonaldetails() {
		return personaldetails;
	}
	public void setPersonaldetails(String personaldetails) {
		this.personaldetails = personaldetails;
	}
	public ResumeBean(String name, String email, String careerobjective,
			String profilesummary, String personaltraits, String academia,
			String technicalqualification, String project, String achievements,
			String personaldetails) {
		super();
		this.name = name;
		this.email = email;
		this.careerobjective = careerobjective;
		this.profilesummary = profilesummary;
		this.personaltraits = personaltraits;
		this.academia = academia;
		this.technicalqualification = technicalqualification;
		this.project = project;
		this.achievements = achievements;
		this.personaldetails = personaldetails;
	}
	public ResumeBean() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
}
