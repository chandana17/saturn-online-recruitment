package com.bvrit.ors.beans;

public class EmployerBean {
	String empname;
	String email;
	String password;
	String designation;
	public String getEmpname() {
		return empname;
	}
	public void setEmpname(String empname) {
		this.empname = empname;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public EmployerBean(String empname, String email, String password,
			String designation) {
		super();
		this.empname = empname;
		this.email = email;
		this.password = password;
		this.designation = designation;
	}
	public EmployerBean() {
		super();
	}
	
}
